from django.db import models
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from user.models import User
from  django.template.loader import get_template
from django.utils.translation import gettext as _
# Create your models here.

class Products(models.Model):
    name = models.CharField(_('name'), max_length=120)
    image = models.ImageField(_('Image'),upload_to='products/', max_length=120)
    description = models.CharField(_('Description'), max_length=120)

    class Meta:
        verbose_name_plural = "Productos"
        verbose_name = 'Producto'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        clients = User.objects.filter(is_active=True, is_staff=False)
        clients_email = []
        for client in clients:
            clients_email.append(client.email)
        template = get_template('product/email.html')
        content = template.render({
            'object': self
        })
        message = Mail(
            from_email='andrealizethpacagui@gmail.com',
            to_emails=clients_email,
            subject="Enviado desde Django-SendGrid",
            html_content=content)
        try:
            sg = SendGridAPIClient("xxxxx")
            sg.send(message)
        except Exception as e:
            print(e.message)

    def __str__(self):
        return self.name