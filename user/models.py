from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    choices = [('Mujer', 'Mujer'),('Hombre', 'Hombre'),('Otro', 'Otro')]
    email = models.EmailField(_('email address'), unique=True)
    phone_number = models.CharField(max_length=30)
    gender = models.CharField(max_length=25, choices=choices, blank=True, null=True)
    city_residence = models.CharField(max_length=30)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def save(self, *args, **kwargs):
        self.username = self.email
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Clientes"
        verbose_name = 'Cliente'