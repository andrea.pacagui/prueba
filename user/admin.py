from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User
from django.utils.translation import gettext_lazy as _

class UserAdmin(UserAdmin):
    list_display = ('email', 'first_name', 'last_name', 'is_staff',)
    fieldsets = (
        (None, {'fields': ('password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'phone_number', 'gender', 'city_residence',)}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser'),
        }),
    )
    
    add_fieldsets = (
    ('Crear nuevo cliente', {
        'fields': ('first_name', 'last_name', 'email', 'phone_number', 'gender', 'city_residence', 'password1', 'password2')
        }),
    )

admin.site.register(User, UserAdmin)